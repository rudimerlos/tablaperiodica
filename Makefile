CC = gcc
CFLAGS = -Wall
OBJ = ./src/advio.o ./src/game.o ./src/main.o
BIN = simbolos

all: $(OBJ)
	$(CC) $(OBJ) -o $(BIN) $(CFLAGS)

clean:
	$(RM) ./src/*.o
