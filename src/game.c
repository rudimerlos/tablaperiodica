/*****************************************************************************\
 *  Copyright (C) 2018  Rudi Merlos Carrión                                  *
 *                                                                           *
 *  This file is part of Underground.                                        *
 *                                                                           *
 *  Underground is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Underground is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Underground.  If not, see <https://www.gnu.org/licenses/>.    *
\*****************************************************************************/

#include "game.h"
#include "advio.h"
#include <string.h>
#include <ctype.h>

const char names[][12] = {"hidrógeno", "helio", "litio", "berilio", "boro",
                          "carbono", "nitrógeno", "oxígeno", "flúor", "neón",
                          "sodio", "magnesio", "aluminio", "silicio",
                          "fósforo", "azufre", "cloro", "argón", "potasio",
                          "calcio", "escandio", "titanio", "vanadio", "cromo",
                          "manganeso", "hierro", "cobalto", "níquel", "cobre",
                          "cinc", "galio", "germanio", "arsénico", "selenio",
                          "bromo", "kriptón", "rubidio", "estroncio",
                          "paladio", "plata", "cadmio", "indio", "estaño",
                          "antimonio", "teluro", "yodo", "xenón", "cesio",
                          "bario", "platino", "oro", "mercurio", "talio",
                          "plomo", "bismuto", "polonio", "astato", "radón",
                          "francio", "radio"};

const char symbols[][3] = {"H", "He", "Li", "Be", "B", "C", "N", "O", "F",
                           "Ne", "Na", "Mg", "Al", "Si", "P", "S", "Cl", "Ar",
                           "K", "Ca", "Sc", "Ti", "V", "Cr", "Mn", "Fe", "Co",
                           "Ni", "Cu", "Zn", "Ga", "Ge", "As", "Se", "Br",
                           "Kr", "Rb", "Sr", "Pd", "Ag", "Cd", "In", "Sn",
                           "Sb", "Te", "I", "Xe", "Cs", "Ba", "Pt", "Au", "Hg",
                           "Tl", "Pb", "Bi", "Po", "At", "Rn", "Fr", "Ra"};

const int position[][2] = {{1, 1}, {1, 18}, {2, 1}, {2, 2}, {2, 13}, {2, 14},
                           {2, 15}, {2, 16}, {2, 17}, {2, 18}, {3, 1}, {3, 2},
                           {3, 13}, {3, 14}, {3, 15}, {3, 16}, {3, 17},
                           {3, 18}, {4, 1}, {4, 2}, {4, 3}, {4, 4}, {4, 5},
                           {4, 6}, {4, 7}, {4, 8}, {4, 9}, {4, 10}, {4, 11},
                           {4, 12}, {4, 13}, {4, 14}, {4, 15}, {4, 16},
                           {4, 17}, {4, 18}, {5, 1}, {5, 2}, {5, 10}, {5, 11},
                           {5, 12}, {5, 13}, {5, 14}, {5, 15}, {5, 16},
                           {5, 17}, {5, 18}, {6, 1}, {6, 2}, {6, 10}, {6, 11},
                           {6, 12}, {6, 13}, {6, 14}, {6, 15}, {6, 16},
                           {6, 17}, {6, 18}, {7, 1}, {7, 2}};

const int valences[][6] = {{-1, 1, -8, -8, -8, -8}, {0, -8, -8, -8, -8, -8},
                           {1, -8, -8, -8, -8, -8}, {2, -8, -8, -8, -8, -8},
                           {-3, 3, -8, -8, -8, -8}, {-4, 2, 4, -8, -8, -8},
                           {-3, 1, 2, 3, 4, 5}, {-2, 2, -8, -8, -8, -8},
                           {-1, -8, -8, -8, -8, -8}, {0, -8, -8, -8, -8, -8},
                           {1, -8, -8, -8, -8, -8}, {2, -8, -8, -8, -8, -8},
                           {3, -8, -8, -8, -8, -8}, {-4, 4, -8, -8, -8, -8},
                           {-3, 3, 5, -8, -8, -8}, {-2, 2, 4, 6, -8, -8},
                           {-1, 1, 3, 5, 7, -8}, {0, -8, -8, -8, -8, -8},
                           {1, -8, -8, -8, -8, -8}, {2, -8, -8, -8, -8, -8},
                           {3, -8, -8, -8, -8, -8}, {2, 3, 4, -8, -8, -8},
                           {2, 3, 4, 5, -8, -8}, {2, 3, 6, -8, -8, -8},
                           {2, 3, 4, 6, 7, -8}, {2, 3, -8, -8, -8, -8},
                           {2, 3, -8, -8, -8, -8}, {2, 3, -8, -8, -8, -8},
                           {1, 2, -8, -8, -8, -8}, {2, -8, -8, -8, -8, -8},
                           {3, -8, -8, -8, -8, -8}, {2, 4, -8, -8, -8, -8},
                           {-3, 3, 5, -8, -8, -8}, {-2, 2, 4, 6, -8, -8},
                           {-1, 1, 3, 5, 7, -8}, {0, -8, -8, -8, -8, -8},
                           {1, -8, -8, -8, -8, -8}, {2, -8, -8, -8, -8, -8},
                           {2, 4, -8, -8, -8, -8}, {1, -8, -8, -8, -8, -8},
                           {2, -8, -8, -8, -8, -8}, {1, 3, -8, -8, -8, -8},
                           {2, 4, -8, -8, -8, -8}, {-3, 3, 5, -8, -8, -8},
                           {-2, 2, 4, 6, -8, -8}, {-1, 1, 3, 5, 7, -8},
                           {0, -8, -8, -8, -8, -8}, {1, -8, -8, -8, -8, -8},
                           {2, -8, -8, -8, -8, -8}, {2, 4, -8, -8, -8, -8},
                           {1, 3, -8, -8, -8, -8}, {1, 2, -8, -8, -8, -8},
                           {1, 3, -8, -8, -8, -8}, {2, 4, -8, -8, -8, -8},
                           {3, 5, -8, -8, -8, -8}, {-2, 2, 4, 6, -8, -8},
                           {-1, 1, 3, 5, 7, -8}, {0, -8, -8, -8, -8, -8},
                           {1, -8, -8, -8, -8, -8}, {2, -8, -8, -8, -8, -8}};

int temp[60];
int temp_index;

void resetTemp(void)
{
    int i;
    for (i = 0; i < 60; ++i) {
        temp[i] = -1;
    }
    temp_index = 0;
}

char menu(void)
{
    char option;
    system("clear");
    do {
        printf("\n\n\tJUEGO DE LA TABLA PERIÓDICA\n");
        printf("    ***********************************\n");
        printf("\nPulsa q para salir\n\n");
        printf("Acierta símbolo, elemento, periodo o valencias (s/e/p/v)? ");
        fflushin();
        scanf("%c", &option);
        option = tolower(option);
        if (option != 's' && option != 'e' && option != 'p' && option != 'v' &&
                option != 'q') {
            printf("Por favor, introduce una opción correcta.\n");
        }
    } while (option != 's' && option != 'e' && option != 'p' && option != 'v'
             && option != 'q');
    return option;
}

int setDificulty(void)
{
    int dificulty;
    do {
        printf("Dificultad: (1)Fácil (2)Medio (3)Difícil? ");
        fflushin();
        scanf("%d", &dificulty);
        if (dificulty < 1 || dificulty > 3)
            printf("Por favor, introduce una opción correcta.\n");
    } while (dificulty < 1 || dificulty > 3);
    return dificulty;
}

int randomElement(void)
{
    int i, pos;
    int end = FALSE;
    while (!end) {
        pos = rand() % 60;
        end = TRUE;
        for (i = 0; i < temp_index; ++i) {
            if (temp[i] == pos) {
                end = FALSE;
                break;
            }
        }
    }
    temp[temp_index++] = pos;
    return pos;
}

int guessSymbol(void)
{
    char symbol[3];
    int pos = randomElement();
    char copy_name[strlen(names[pos])];
    strcpy(copy_name, names[pos]);
    capitalizeStr(copy_name);
    system("clear");
    printf("\n%s: ", copy_name);
    getStr(symbol, 3);
    tolowerStr(symbol);
    symbol[0] = toupper(symbol[0]);
    if (!strcmp(symbols[pos], symbol)) {
        puts("Correcto!!!");
        return TRUE;
    }
    printf("\nError\n%s => %s\n", copy_name, symbols[pos]);
    return FALSE;
}

int guessName(void)
{
    char name[12];
    int pos = randomElement();
    char copy_name[strlen(names[pos])];
    strcpy(copy_name, names[pos]);
    capitalizeStr(copy_name);
    system("clear");
    printf("\n%s: ", symbols[pos]);
    getStr(name, 12);
    tolowerStr(name);
    if (!strcmp(names[pos], name)) {
        puts("Correcto!!!");
        return TRUE;
    }
    printf("\nError\n%s => %s\n", symbols[pos], copy_name);
    return FALSE;
}

int guessPosition(void)
{
    int period, group;
    int pos = randomElement();
    char copy_name[strlen(names[pos])];
    strcpy(copy_name, names[pos]);
    capitalizeStr(copy_name);
    system("clear");
    printf("\n%s (%s)\n", copy_name, symbols[pos]);
    printf("Periodo y grupo: ");
    fflushin();
    scanf("%d%d", &period, &group);
    if (position[pos][0] == period && position[pos][1] == group) {
        puts("Correcto!!!");
        return TRUE;
    }
    printf("\nError\nPeriodo %d y grupo %d\n", position[pos][0], position[pos][1]);
    return FALSE;
}

int guessValence(void)
{
    int i, j, aux;
    char valence_str[20];
    int factor, cont = 0;
    int temp[10] = { -8 };
    int pos = randomElement();
    char copy_name[strlen(names[pos])];
    strcpy(copy_name, names[pos]);
    capitalizeStr(copy_name);
    system("clear");
    printf("\n%s (%s)\n", copy_name, symbols[pos]);
    printf("Valencia: ");
    getStr(valence_str, 20);
    // Convert string to int array
    for (i = 0, cont = 0; i < strlen(valence_str); ++i) {
        if (valence_str[i] == '-')
            factor = -1;
        else if (valence_str[i] >= '0' && valence_str[i] <= '9')
            temp[cont++] = (valence_str[i] - '0') * factor;
        else
            factor = 1;
    }
    // Create new int array
    int valence[cont];
    for (i = 0; i < cont; ++i) {
        valence[i] = temp[i];
    }
    // Order list
    for (i = 0; i < cont; i++) {
        for (j = i; j < cont; j++) {
            if (valence[i] > valence[j]){
                aux = valence[i];
                valence[i] = valence[j];
                valence[j] = aux;
            }
        }
    }
    // Compare lists
    for (i = 0; i < cont; ++i) {
        if (valence[i] != valences[pos][i]) {
            // Print list
            printf("\nError\nValencia: ");
            for (i = 0; i < 6; ++i) {
                if (valences[pos][i] > -4 && valences[pos][i] < 7)
                    printf("%d ", valences[pos][i]);
            }
            putchar('\n');
            return FALSE;
        }
    }
    puts("Correcto!!!");
    return TRUE;
}
