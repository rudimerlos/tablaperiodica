/*****************************************************************************\
 *  Copyright (C) 2018  Rudi Merlos Carrión                                  *
 *                                                                           *
 *  This file is part of Underground.                                        *
 *                                                                           *
 *  Underground is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Underground is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Underground.  If not, see <https://www.gnu.org/licenses/>.    *
\*****************************************************************************/

#include "advio.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>


/*! \brief Clear stdin buffer in UNIX
 *
 *  Generates a loop that passes through each character that remains in the
 *  input to clean the stdin buffer.
 *
 * \return void
 */
void fflushin(void)
{
    int d, i;
    d = stdin->_IO_read_end - stdin->_IO_read_ptr;
    for(i=0; i<d; i++)
        getchar();
}


/*! \brief Close string with '\0' character
 *
 *  Add a '\0' character to the end of the string passed as an argument.
 *
 * \param *st Pointer to string char
 * \return void
 */
void closeStr(char *st)
{
    if(st[strlen(st)-1] == '\n')
        st[strlen(st)-1] = '\0';
}


/*! \brief Gets a string
 *
 *  Gets a string from stdin
 *
 * \param *st Pointer to string char
 * \param len Maximum number of characters
 * \return void
 */
void getStr(char *st, short len)
{
    fflushin();
    fgets(st, len, stdin);
    closeStr(st);
    fflushin();
}


/*! \brief Convert string to uppercase
 *
 *  Convert all characters of string st to uppercase
 *
 * \param *st Pointer to string char
 * \return void
 */
void toupperStr(char *st)
{
    do {
        if (*st != -61)
            *st = toupper(*st);
        else
            if (*++st > -100)
                *st -= 32;
    }while (*++st != '\0');
}


/*! \brief Convert string to lowercase
 *
 *  Convert all characters of string st to lowercase
 *
 * \param *st Pointer to string char
 * \return void
 */
void tolowerStr(char *st)
{
    do {
        if (*st != -61)
            *st = tolower(*st);
        else
            if (*++st <= -100)
                *st += 32;
    }while (*++st != '\0');
}


/*! \brief Convert first character to uppercase
 *
 *  Convert first character of string st to uppercase
 *
 * \param *st Pointer to string char
 * \return void
 */
void capitalizeStr(char *st) {
    *st = toupper(*st);
}


/*! \brief Waits for '\n' character
 *
 *  Shows a message "Press ENTER to continue..." and waits a '\n' character to
 *  continue.
 *
 * \return void
 */
void enterToContinue(void)
{
    printf("\nPress ENTER to continue...");
    fflushin();
    while(getchar() != '\n');
}
