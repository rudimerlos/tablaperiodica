/*****************************************************************************\
 *  Copyright (C) 2018  Rudi Merlos Carrión                                  *
 *                                                                           *
 *  This file is part of Underground.                                        *
 *                                                                           *
 *  Underground is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Underground is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Underground.  If not, see <https://www.gnu.org/licenses/>.    *
 *                                                                           *
 *                                                                           *
 *                          List of functions                                *
 *                         -------------------                               *
 *                                                                           *
 *  void closeStr(char*)                                                     *
 *  void enterToContinue(void)                                               *
 *  void fflushin(void)                                                      *
 *  void getStr(char*, short)                                                *
 *                                                                           *
 \****************************************************************************/

#ifndef ADVIO_H
#define ADVIO_H

#define TRUE 1
#define FALSE 0


/*! \brief Clear stdin buffer in UNIX
 *
 *  Generates a loop that passes through each character that remains in the
 *  input to clean the stdin buffer.
 *
 * \return void
 */
void fflushin(void);

/*! \brief Close string with '\0' character
 *
 *  Add a '\0' character to the end of the string passed as an argument.
 *
 * \param *st Pointer to string char
 * \return void
 */
void closeStr(char *st);

/*! \brief Gets a string
 *
 *  Gets a string from stdin
 *
 * \param *st Pointer to string char
 * \param len Maximum number of characters
 * \return void
 */
void getStr(char *st, short len);

/*! \brief Convert string to uppercase
 *
 *  Convert all characters of string st to uppercase
 *
 * \param *st Pointer to string char
 * \return void
 */
void toupperStr(char *st);

/*! \brief Convert string to lowercase
 *
 *  Convert all characters of string st to lowercase
 *
 * \param *st Pointer to string char
 * \return void
 */
void tolowerStr(char *st);

/*! \brief Convert first character to uppercase
 *
 *  Convert first character of string st to uppercase
 *
 * \param *st Pointer to string char
 * \return void
 */
void capitalizeStr(char *st);

/*! \brief Waits for '\n' character
 *
 *  Shows a message "Press ENTER to continue..." and waits a '\n' character to
 *  continue.
 *
 * \return void
 */
void enterToContinue(void);

#endif /* ADVIO_H */
