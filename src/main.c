/*****************************************************************************\
 *  Copyright (C) 2018  Rudi Merlos Carrión                                  *
 *                                                                           *
 *  This file is part of Underground.                                        *
 *                                                                           *
 *  Underground is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Underground is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Underground.  If not, see <https://www.gnu.org/licenses/>.    *
\*****************************************************************************/

#include "game.h"
#include "advio.h"

int main(int argc, char *argv[])
{
    // ---------- Init variables -----------
    int end = FALSE;
    char option;
    int dificulty, attemts;
    int win;
    int i;

    srand(time(NULL));

    // ---------- Main loop ----------
    while(!end)
    {
        // ---------- Menu ----------
        if ((option = menu()) == 'q') {
            end = TRUE;
            break;
        } else {
            dificulty = setDificulty();
            if (dificulty == 1)
                attemts = 10;
            else if (dificulty == 2)
                attemts = 30;
            else
                attemts = 60;
        }

        win = 0;
        system("clear");
        resetTemp();

        // ---------- Game ----------
        switch (option) {
            case 's':
                for (i = 0; i < attemts; ++i) {
                    win += guessSymbol();
                    sleep(1);
                }
                break;
            case 'e':
                for (i = 0; i < attemts; ++i) {
                    win += guessName();
                    sleep(1);
                }
                break;
            case 'p':
                for (i = 0; i < attemts; ++i) {
                    win += guessPosition();
                    sleep(1);
                }
                break;
            case 'v':
                for (i = 0; i < attemts; ++i) {
                    win += guessValence();
                    sleep(1);
                }
                break;
        }

        // ---------- Results ----------
        system("clear");
        printf("\nAciertos: %d\nErrores: %d\n", win, attemts - win);
        sleep(3);
    }

    printf("\nAdiós!!!\n");

    return 0;
}
