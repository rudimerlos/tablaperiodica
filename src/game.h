/*****************************************************************************\
 *  Copyright (C) 2018  Rudi Merlos Carrión                                  *
 *                                                                           *
 *  This file is part of Underground.                                        *
 *                                                                           *
 *  Underground is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by     *
 *  the Free Software Foundation, either version 3 of the License, or        *
 *  (at your option) any later version.                                      *
 *                                                                           *
 *  Underground is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *  GNU General Public License for more details.                             *
 *                                                                           *
 *  You should have received a copy of the GNU General Public License        *
 *  along with Underground.  If not, see <https://www.gnu.org/licenses/>.    *
\*****************************************************************************/

#ifndef GAME_H
#define GAME_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

void resetTemp(void);
char menu(void);
int setDificulty(void);
int randomElement(void);
int guessSymbol(void);
int guessName(void);
int guessPosition(void);
int guessValence(void);

#endif /* GAME_H */
