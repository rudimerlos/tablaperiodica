# TABLA PERIÓDICA

---

### DESCRIPCIÓN

Tabla Periódica es un juego en modo consola para aprenderse la tabla periodica.
Nombres de de elementos, símbolos, posición del elemento en la tabla (periodo y
grupo) y valencias.

---

#### INSTALACIÓN

Tabla Periódica esta escrito en C.
Compilar con:

    $ make && make clean

y ejecutar con:

    $ ./simbolos

---

#### AUTOR

Programación por Rudi Merlos Carrión.

---

#### LICENCIA

Copyright (C) 2018  Rudi Merlos Carrión

This file is part of Tabla Periodica.

Tabla Periodica is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Tabla Periodica is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Tabla Periodica.  If not, see <https://www.gnu.org/licenses/>.
